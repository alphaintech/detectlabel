#include "opencv2/opencv.hpp"
#include "opencv2/highgui.hpp"

using namespace std;
using namespace cv;


void PaintContourOfFigure(IplImage *image) {
	
	cvRectangle(image, cvPoint(20, 50), cvPoint(150,180), CV_RGB(255, 0, 0), 1, 8);
	cvShowImage("Test", image);
	cvWaitKey(0);
	cvReleaseImage(&image);
}

